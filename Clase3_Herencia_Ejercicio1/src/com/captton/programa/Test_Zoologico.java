package com.captton.programa;
import com.captton.zoologico.*;

public class Test_Zoologico {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Pajaro pajaro1 = new Pajaro("Zoe", 30, 0.300f);
		Perro perro1 = new Perro("Firulais", 13, 6f);
		Pez pez1 = new Pez("Tiburoncin", 15, 0.100f);

		System.out.println("/////Perro/////");
		
		System.out.println("\n");
		
		System.out.println("///Peso del perro original///");
		System.out.println(perro1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Aumenta Peso///");
		perro1.comer(2f);
		System.out.println(perro1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Aumenta Peso///");
		perro1.comer(2f);
		System.out.println(perro1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Edad Original///");
		System.out.println(perro1.getEdad());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Rejuvenece///");
		perro1.ladran();
		System.out.println(perro1.getEdad());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Rejuvenece///");
		perro1.ladran();
		System.out.println(perro1.getEdad());
		System.out.println("//////");
		
		System.out.println("----------------------");

		System.out.println("/////Pajaro/////");
		
		System.out.println("///Peso del perro original///");
		System.out.println(pajaro1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Aumenta Peso///");
		pajaro1.comer(2f);
		System.out.println(pajaro1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Aumenta Peso///");
		pajaro1.comer(2f);
		System.out.println(pajaro1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Edad Original///");
		System.out.println(pajaro1.getEdad());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Rejuvenece///");
		pajaro1.vuelan();
		System.out.println(pajaro1.getEdad());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Rejuvenece///");
		pajaro1.vuelan();
		System.out.println(pajaro1.getEdad());
		System.out.println("//////");
		
		
		System.out.println("----------------------");

		System.out.println("/////Pez/////");
		
		System.out.println("\n");
		
		System.out.println("///Peso del pez original///");
		System.out.println(pez1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Aumenta Peso///");
		pez1.comer(2f);
		System.out.println(pez1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Aumenta Peso///");
		pez1.comer(2f);
		System.out.println(pez1.getPeso());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Edad Original///");
		System.out.println(pez1.getEdad());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Rejuvenece///");
		pez1.nadan();
		System.out.println(pez1.getEdad());
		System.out.println("//////");
		
		System.out.println("\n");
		
		System.out.println("///Rejuvenece///");
		pez1.nadan();
		System.out.println(pez1.getEdad());
		System.out.println("//////");
		
		
	}

}
