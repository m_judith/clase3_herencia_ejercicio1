package com.captton.zoologico;

public abstract class Mascota {

	protected String nombre;
	protected float edad;
	protected float peso;
	protected final float AUMENTO_EDAD = 10f;
	
	
	
	public Mascota(String nombre, float edad, float peso) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.peso = peso;
	}

	public abstract void comer(float cantComida);
	
	public void rejuvenece() {
		this.edad = (this.edad - ((this.edad*this.AUMENTO_EDAD)/100));
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getEdad() {
		return edad;
	}

	public void setEdad(float edad) {
		this.edad = edad;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	
	
}
