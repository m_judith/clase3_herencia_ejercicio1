package com.captton.zoologico;

public class Pajaro extends Mascota {
	
	private final float AUMENTO = 30f;
	private float contVuelan= 0f;

	public Pajaro(String nombre, float edad, float peso) {
		super(nombre, edad, peso);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void comer(float cantComida) {
		this.peso = this.peso + ((this.peso*this.AUMENTO)/100);
	}

	
	public void vuelan() {
		System.out.println("Desde Arriba todo es bello!");
		this.rejuvenece();
	}
	
}
