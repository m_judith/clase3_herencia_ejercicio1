package com.captton.zoologico;

public class Pez extends Mascota {
	
	private final float AUMENTO = 70f;
	private  float contNadan = 0f;

	public Pez(String nombre, float edad, float peso) {
		super(nombre, edad, peso);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void comer(float cantComida) {
		this.peso = this.peso + ((this.peso*this.AUMENTO)/100);
	}
	
	public void nadan() {
		System.out.println("Que lindo es nadar");
		this.rejuvenece();
	}
	
}
